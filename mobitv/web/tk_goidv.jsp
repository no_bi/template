<%-- 
    Document   : index
    Created on : Dec 14, 2016, 4:10:17 PM
    Author     : ManhThang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%--<%@include file="header1.jsp" %>--%>
    <body>
        <%@include file="header.jsp" %>
        <div style="clear: both"></div>
        <%--@include file="layout.jsp" --%>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 title">
                    <fmt:message key="goi_dich_vu" bundle="${msg}" />
                </div>
            </div>
            <div class="row">
                <div class="table-responsive">    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="background-color: #b1b1b1"> <fmt:message key="dich_vu" bundle="${msg}" /></th>
                                <th style="background-color: #979797"> <fmt:message key="thoi_gian" bundle="${msg}" /></th>
                                <th  style="background-color: #b1b1b1"> <fmt:message key="gia_tien" bundle="${msg}" /></th>
                                <th style="background-color: #979797"> <fmt:message key="het_han" bundle="${msg}" /></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Live TV</td>
                                <td style="color: #9d2d01">30/03/2017</td>
                                <td style="color: #1e5f91">15.000vnd</td>
                                <td style="color: #9d2d01">30/03/2018</td>
                            </tr>
                            <tr>
                                <td>VOD</td>
                                <td style="color: #9d2d01">30/03/2017</td>
                                <td style="color: #1e5f91">15.000vnd</td>
                                <td style="color: #9d2d01">30/03/2018</td>
                            </tr>
                            <tr>
                                <td>CLIP</td>
                                <td style="color: #9d2d01">30/03/2017</td>
                                <td style="color: #1e5f91">15.000vnd</td>
                                <td style="color: #9d2d01;white-space:nowrap">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                30/03/2018
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                               <button class="btn btn-primary btnBuy btn-buypackage" style=" white-space: normal;color: #fff"> <fmt:message key="mua_goi" bundle="${msg}" /></button>
                                               &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                               <button class="btn btn-danger btnCancel text-nowrap btn-buypackage" style=" white-space: normal;color: #fff"><fmt:message key="huy_goi" bundle="${msg}" /></button>                                            
                                            </td>
                                        </tr>
                                    </table>
<!--                                    <div class="row"style="margin-left:-5px">
                                        <div class="col-xs-12">30/03/2018</div>
                                    </div>
                                    <div class="row" style="margin-left:-5px">
                                        <div class="col-xs-6 ">
                                            <button class="btn btn-primary btnBuy btn-buypackage" style=" white-space: normal;color: #fff"> <fmt:message key="mua_goi" bundle="${msg}" /></button>
                                            
                                        </div>
                                        <div class="col-xs-6 " >
                                            <button class="btn btn-danger btnCancel text-nowrap btn-buypackage" style=" white-space: normal;color: #fff"><fmt:message key="huy_goi" bundle="${msg}" /></button>                                            
                                        </div>
                                    </div>-->
                                </td>                                
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>



        </div>
        <%@include file="footer.jsp" %>
    </body>
    <!--<img class="scrollup" src="img/ico_up.png" >-->
</html>

<script type="text/javascript">
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".glyphicon-menu-down").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".glyphicon-menu-up").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
    });
</script>