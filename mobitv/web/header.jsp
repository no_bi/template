<%-- 
    Document   : header
    Created on : Mar 24, 2017, 9:33:47 AM
    Author     : TILI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <title>MobiTV</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />




        <script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!--<script src="js/jquery-ui-1.10.3.js" type="text/javascript"></script>-->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/main_1.css" rel="stylesheet" type="text/css"/>
        <link href="css/tungpm.css" rel="stylesheet" type="text/css"/>        
        <link href="css/owl.theme.css" rel="stylesheet" type="text/css"/>
        <link href="css/owl.transitions.css" rel="stylesheet" type="text/css"/>


        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
              rel = "stylesheet">
        <!--<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>-->
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/5.10.2/video.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/3.0.2/videojs-contrib-hls.js"></script>
    </head>
    <div class="container">
        <%if (request.getParameter("optLang") != null) {
                session.setAttribute("language", request.getParameter("optLang"));
            }else{session.setAttribute("language", "en_US");}%>        
        <fmt:setLocale value="${language}" />
        <fmt:setBundle basename="Messages" var="msg" />
        <div class="row  top_search" >
            <div class="col-xs-4 col-md-4 col-sm-4 top_search" >
                <img style="padding-top: 4px" src="img/public_items/mettv_logo.png" alt="" class="img-responsive "/>
            </div>
            <div class="col-xs-8 col-md-8 col-sm-8 search top_search" style="padding-top: 5px;padding-bottom: 5px;" >
                <form action="">
                    <div class="has-feedback">
                        <label for="search" class="sr-only">Search</label>
                        <input type="text" class="form-control input-sm inputSeach" 
                               name="search" id="search" placeholder="<fmt:message key="top_text_search" bundle="${msg}" />">
                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                    </div>
                </form>
            </div>            
        </div>

        <div class="row top-menu">
            <div 
                <%if (request.getParameter("cate") == null || request.getParameter("cate").equals("1")) {%>
                class="col-xs-2 menu_active  "
                <%} else {%>class="col-xs-2 menu_inactive"<%}%>
                >
                <a href="index.jsp?cate=1">
                    <img class="center-block " style="padding-top: 4px;" src="img/public_items/home-5-24.png" />
                </a>
            </div>
            <div 
                <%if (request.getParameter("cate") != null && request.getParameter("cate").equals("2")) {%>
                class="col-xs-3 menu_active text-menu text-center"
                <%} else {%>class="col-xs-3 menu_inactive  text-menu text-center"<%}%>
                >                
                <a  href="livetv.jsp?cate=2" ><fmt:message key="header_live" bundle="${msg}" /></a></div>
            <div 
                <%if (request.getParameter("cate") != null && request.getParameter("cate").equals("3")) {%>
                class="col-xs-2 menu_active text-menu text-center"
                <%} else {%>class="col-xs-2 menu_inactive  text-menu text-center"<%}%>
                >
                <a href="vod.jsp?cate=3" ><fmt:message key="header_vod" bundle="${msg}" /></a></div>
            <div 
                <%if (request.getParameter("cate") != null && request.getParameter("cate").equals("4")) {%>
                class="col-xs-3 menu_active text-menu text-center"
                <%} else {%>class="col-xs-3 menu_inactive  text-menu text-center"<%}%>                
                ><a href="clip.jsp?cate=4" ><fmt:message key="header_clip" bundle="${msg}" /></a></div>
            <div  <%if (request.getParameter("cate") != null && request.getParameter("cate").equals("5")) {%>
                class="col-xs-2 menu_active  "
                <%} else {%>class="col-xs-2 menu_inactive"<%}%>
                ><a href="tk_goidv.jsp?cate=5"><img class="center-block "style="padding-top: 4px;" src="img/public_items/user24.png" alt=""/></a></div>        
        </div>     
        <div class="row" >
            <div class="panel-group">
                <div class="panel panel-default" style="border: 0px;">

                    <div id="collapse_menu" class="panel-collapse collapse bg-color-orange">
                        <div class="panel-body noneBorder" style="padding-top: 0px;">
                            <!--livetv-->
                            <%if (request.getParameter("cate") != null && request.getParameter("cate").equals("2")) {%>
                            <div class="row top5">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-8">                                    
                                    <ul class="pager no-margin text-left">
                                        <li><a href="livetvlichphatsong.jsp?cate=2" class="mypaper " style="background-color: #81391d;width: 100%;"><fmt:message key="sub_menu" bundle="${msg}" /></a></li>            
                                    </ul>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>
                            <%}%>
                            <!--end livetv-->
                            <!--vod-->
                            <%if (request.getParameter("cate") != null && request.getParameter("cate").equals("3")) {%>
                            <div class="row top10">  
                                <div class="col-xs-1"></div>
                                <div class="col-xs-10 text-center" style="color: #FFF">
                                    <ul class="no-margin text-left">
                                        <li><a href="#"  style="color: #81391d;width: 100%;font-weight: bold;">THỂ LOẠI</a></li>            
                                    </ul>
                                </div>
                                <div class="col-xs-1"></div>
                            </div>
                            <div class="row top5">
                                <div class="col-xs-1"></div>
                                <div class="col-xs-10 text-center" style="color: #FFF">
                                    <ul class="no-margin text-left">
                                        <li><a href="#"  style="color: #81391d;width: 100%;font-weight: bold;">PHIM MỚI</a></li>            
                                    </ul>
                                </div>
                                <div class="col-xs-1"></div>
                            </div>
                            <div class="row top5">
                                <div class="col-xs-1"></div>
                                <div class="col-xs-10 text-center" style="color: #FFF">
                                    <ul class="no-margin text-left">
                                        <li><a href="#"  style="color: #81391d;width: 100%;font-weight: bold;">PHIM ĐẶC SẮC</a></li>            
                                    </ul>
                                </div>
                                <div class="col-xs-1"></div>
                            </div>
                            <div class="row top5">
                                <div class="col-xs-1"></div>
                                <div class="col-xs-10 text-center" style="color: #FFF">
                                    <ul class="no-margin text-left">
                                        <li><a href="#"  style="color: #81391d;font-weight: bold;width: 100%;">PHIM CỦA BẠN</a></li>            
                                    </ul>
                                </div>
                                <div class="col-xs-1"></div>
                            </div>
                            <%}%>
                            <!--end vod-->

                            <%if (request.getParameter("cate") != null && request.getParameter("cate").equals("5")) {%>
                            <div class="row top10">  
                                <div class="col-xs-1"></div>
                                <div class="col-xs-10 text-center" style="color: #FFF">
                                    XIN CHÀO THUÊ BAO 0902.XXX.XXX
                                </div>
                                <div class="col-xs-1"></div>
                            </div>
                            <div class="row top5 nonePadding">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-8">                                    
                                    <ul class="pager no-margin text-left">
                                        <li><a href="#" class="mypaper " 
                                               style="background-color: #81391d;width: 100%;color: #fff"
                                               data-toggle="modal" data-target="#myModal"
                                               ><fmt:message key="ngon_ngu" bundle="${msg}" /></a></li>            
                                    </ul>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>                            
                            <div class="row top5">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-8">                                    
                                    <ul class="pager no-margin text-left">
                                        <li><a href="#" class="mypaper " style="background-color: #81391d;width: 100%;color: #fff"><fmt:message key="goi_dich_vu" bundle="${msg}" /></a></li>            
                                    </ul>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>
                            <div class="row top5">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-8">
                                    <ul class="pager no-margin">
                                        <li><a href="#" class="mypaper " style="background-color: #81391d;width: 100%;color: #fff"><fmt:message key="lich_su_giao_dich" bundle="${msg}" /></a></li>            
                                    </ul>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>
                            <%}%>

                        </div>
                        <!--<div class="panel-footer">Panel Footer</div>-->
                    </div>
                    <div style="background-color: #d16d46;height: 12px;padding:0;border-radius: 0px;border: 0px;" 
                         class="panel-heading text-center" >
                        <span style="padding-top: 0px;">
                            <a data-toggle="collapse" href="#collapse_menu">
                                <i  style="top: -3px;color: #fff" class="glyphicon glyphicon-menu-down white" ></i>
                            </a>
                        </span>                        
                    </div>
                </div>
            </div>
        </div>        
        <div class="top10"></div>

        <!-- Modal -->
        <div id="myModal" class="modal fade " style="top: 10%;" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header bg-color-orange" style="color: #fff;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><fmt:message key="chon_ngon_ngu" bundle="${msg}" /></h4>
                    </div>
                    <form >
                        <div class="modal-body">                        
                            <div class="row top5 nonePadding">                                

                                <div class="col-xs-4">                                                                       
                                    <input type="hidden" value="5" name="cate"/>
                                    <label><input type="radio" name="optLang"
                                                  value="lo_LA" ${language == 'lo_LA' ? 'checked' : ''}>Laos</label>
                                </div>
                                <div class="col-xs-4">
                                    <label><input type="radio" name="optLang" 
                                                  value="vi_VN" ${language == 'vi_VN' ? 'checked' : ''}>VN</label>
                                </div>
                                <div class="col-xs-4">
                                    <label><input type="radio" name="optLang" 
                                                  value="en_US" ${language == 'en_US' ? 'checked' : ''}>EN</label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"  class="btn btn-primary" >Đồng ý</button>&nbsp;
                            <!--<a onclick="changeLanguage('en');" class="btn btn-primary" >Đồng ý</a>&nbsp;-->
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</html>
<script type="text/javascript">
    
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".glyphicon-menu-down").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
    }).on('hide.bs.collapse', function () {
        $(this).parent().find(".glyphicon-menu-up").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
    });

//    $("div").on("click", "a", function (event) {
////        $(event.delegateTarget).css("background-color", "green");
//        $(this).toggleClass('menu_active');
//    });
</script>
