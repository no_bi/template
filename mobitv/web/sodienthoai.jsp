<%-- 
    Document   : otp
    Created on : Mar 24, 2017, 10:51:33 AM
    Author     : TILI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <%@include file="header.jsp" %>
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12 top30 bottom100 text-center">
                        <p class="textBold">NHẬP SỐ ĐIỆN THOẠI</p>
                    </div>
                    <div class="col-xs-12 bottom100">
                        <div class="form-group">
                            <label class="title" for="maOTP">Nhập Số điện thoại</label>
                            <input type="text" class="form-control inputNoneBoder" placeholder="Số điện thoại">
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <button class="btn btn3">Tiếp theo</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
