/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hatviet.mobitv.billing.wap.webservice.client;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hatviet.mobitv.content.live.Cate;
import com.soft.util.Constant;
import com.soft.util.GetJsonData;
import java.util.List;
import org.json.simple.parser.JSONParser;

/**
 *
 *
 * @author tung
 */
public class Content {

    public static Cate getLive(String cateId) throws Exception {
        Cate ret;
        String check = "";
        try {
            GetJsonData getJsonCheck = new GetJsonData();            
            String checkJson = getJsonCheck.readUrl(Constant.URL_LIVE.replace("ID1", cateId));
            Gson gson = new Gson();
            ret = gson.fromJson(checkJson, Cate.class);
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
