/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soft.util;

/**
 *
 * @author ManhThang
 */
public class Constant {

    public Constant() {
    }
    public static final String URL_LIVE = Config.getProperty("API_ROOT") + "/account/getLive?cateId=ID1";
//    public static final String URL_MENU = Config.getProperty("API_ROOT") + "getCategoryList?parentId=0&contentType=&homePage=";
//    public static final String URL_LIST_AUDIO = Config.getProperty("API_ROOT") + "getMediaList?cateId=ID&contentType=1&type=&page=PAGE&pageSize=12&orderType&txt=TXT&msisdn=";
//    public static final String URL_LIST_VIDEO = Config.getProperty("API_ROOT") + "getMediaList?cateId=ID&contentType=2&type=&page=PAGE&pageSize=12&orderType&txt=TXT&msisdn=";
//    public static final String URL_LIST_STORY = Config.getProperty("API_ROOT") + "getStoryList?cateId=ID&type=&page=PAGE&pageSize=12&orderType&txt=TXT&msisdn";
//    public static final String URL_CHECK_MSISDN = Config.getProperty("API_ROOT") + "getMemberStatus?msisdn=";
//    public static final String URL_AUDIO_TOP = Config.getProperty("API_ROOT") + "getMediaList?cateId=&contentType=1&type=&page=&pageSize&orderType&txt=&msisdn=";
//    public static final String URL_VIDEO_HOT = Config.getProperty("API_ROOT") + "getMediaList?cateId=&contentType=2&type=&page=&pageSize&orderType&txt=&msisdn=";
//    public static final String URL_CHECK_CONTENT = Config.getProperty("API_ROOT") + "checkOrderedContent?msisdn=SDT&contentId=ID&contentType=TYPE";
//    public static final String URL_MUA_LE = Config.getProperty("API_ROOT") + "buyContent";
//    public static final String URL_STORY_DETAIL = Config.getProperty("API_ROOT") + "getStoryById?id=";
//    public static final String URL_MEDIA_DETAIL = Config.getProperty("API_ROOT") + "getMediaById?id=";
//    public static final String URL_COMMENT_LIST = Config.getProperty("API_ROOT") + "getCommentList?msisdn=&contentId=ID&contentType=TYPE&page=1&pageSize=10";
//    public static final String URL_COMMENT_POST = Config.getProperty("API_ROOT") + "addComment";
//    public static final String URL_LOVE = Config.getProperty("API_ROOT") + "addFavorite?msisdn=SDT&contentId=ID&contentType=TYPE";
//    public static final String URL_LOVE_LIST = Config.getProperty("API_ROOT") + "getFavoriteList?msisdn=SDT&page=&pageSize";
    
    public static final String URL_MENU = Config.getProperty("API_ROOT") + "getCategoryList?parentId=0";
    public static final String URL_SUB_CATE = Config.getProperty("API_ROOT") + "getCategoryList?parentId=ID";
    public static final String URL_LIST_AUDIO = Config.getProperty("API_ROOT") + "getMediaList?cateId=ID&page=PAGE&pageSize=12&orderType=&txt=TXT&msisdn=";
    public static final String URL_LIST_AUDIO_HOME = Config.getProperty("API_ROOT") + "getMediaList?rootCateId=ID&page=PAGE&pageSize=10&orderType=&txt=TXT&msisdn=";
    public static final String URL_LIST_VIDEO = Config.getProperty("API_ROOT") + "getMediaList?cateId=ID&page=PAGE&pageSize=12&orderType=&txt=TXT&msisdn=";
    public static final String URL_LIST_STORY = Config.getProperty("API_ROOT") + "getStoryList?cateId=ID&page=PAGE&pageSize=12&orderType=&txt=TXT&msisdn";
    public static final String URL_CHECK_MSISDN = Config.getProperty("API_ROOT") + "getMemberStatus?msisdn=";
    public static final String URL_AUDIO_TOP = Config.getProperty("API_ROOT") + "getMediaList?cateId=&page=&pageSize&orderType&txt=&msisdn=";
    public static final String URL_VIDEO_HOT = Config.getProperty("API_ROOT") + "getMediaList?cateId=&page=&pageSize&orderType&txt=&msisdn=";
    public static final String URL_CHECK_CONTENT = Config.getProperty("API_ROOT") + "checkOrderedContent?msisdn=SDT&contentId=ID&contentType=TYPE";
    public static final String URL_MUA_LE = Config.getProperty("API_ROOT") + "buyContent";
    public static final String URL_STORY_DETAIL = Config.getProperty("API_ROOT") + "getStoryById?id=";
    public static final String URL_MEDIA_DETAIL = Config.getProperty("API_ROOT") + "getMediaById?id=";
    public static final String URL_COMMENT_LIST = Config.getProperty("API_ROOT") + "getCommentList?msisdn=&contentId=ID&contentType=TYPE&page=1&pageSize=10";
    public static final String URL_COMMENT_POST = Config.getProperty("API_ROOT") + "addComment";
    public static final String URL_LOVE = Config.getProperty("API_ROOT") + "addFavorite?msisdn=SDT&contentId=ID&contentType=TYPE";
    public static final String URL_LOVE_LIST = Config.getProperty("API_ROOT") + "getFavoriteList?msisdn=SDT&page=&pageSize";
    
    public static final String URL_UPDATE_VIEWCOUNT = Config.getProperty("API_ROOT") + "updateViewCount?contentId=";
    public static final String URL_LIST_SEARCH = Config.getProperty("API_ROOT") + "getMediaList?rootCateId=ID&page=PAGE&pageSize=12&orderType=&txt=TXT&msisdn=";
//    MEDIA CONTENT
    public static final String MEDIA_CONTENT_URL = Config.getProperty("MEDIA_CONTENT") + "ID.";
    public static final String URL_GET_COMMON = Config.getProperty("API_ROOT") + "getCommonList";
    
}
