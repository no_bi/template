package com.soft.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class GetJsonData {

    /**
     *
     *
     *
     */


    public String readUrl(String urlString){
//        System.out.println("URLLLLLL=>>>>>"+urlString);
        InputStreamReader isr = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            isr = new InputStreamReader(url.openStream(),
                    "UTF-8");
            reader = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1) {
                buffer.append(chars, 0, read);
            }
            return buffer.toString();
        } catch (Exception ex) {
//            Util.alert("Wap read URL", ex.getMessage());
            ex.printStackTrace();
            return null;
        } finally {
            try {
            if (reader != null) {
                reader.close();
            }
            if(isr != null){
                isr.close();
            }    
            } catch (Exception e) {
            }
            
        }

    }

    //final static Logger logger = Logger.getLogger(GetJsonData.class);


}
