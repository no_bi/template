package com.soft.util;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonUtil {

    public String getFieldName(String jsonString, String fieldName) {
        if (jsonString.length() > 0) {
            JSONParser parser = new JSONParser();
            try {
                //System.out.println("TestNhat" + jsonString);
                Object obj = parser.parse(jsonString);
                JSONObject jsonObject = (JSONObject) obj;
                String result = "";
                if (jsonObject.get(fieldName) != null) {
                    result = jsonObject.get(fieldName).toString();
                }
                //System.out.println(result);
                return result;
            } catch (Exception e) {
                // TODO Auto-generated catch block
//            Util.alert("Wap read JSON", e.getMessage());
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }

    }

    public ArrayList<String> getRowJson(String jsonArray) {
        JSONParser parser = new JSONParser();
        ArrayList<String> listJson = new ArrayList<String>();
        Object obj;
        try {
            obj = parser.parse(jsonArray);
            JSONArray msg = (JSONArray) obj;
            Iterator<Object> iterator = msg.iterator();
            while (iterator.hasNext()) {
                listJson.add(iterator.next().toString());
            }
            return listJson;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }

    public static void main(String[] args) {
        JsonUtil util = new JsonUtil();
        System.out.println("{\"School\":1,\"schoolId\":\"NH\"}");

    }

    public JsonUtil() {

    }

    public void method1() {
        System.out.println("2222222222");
    }
}
