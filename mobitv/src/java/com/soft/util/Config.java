/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soft.util;

import java.util.Enumeration;
import java.util.ResourceBundle;

/**
 *
 * @author ThangNm
 */
public class Config {

    public static String getProperty(String key) {
        return getStandardServiceCode(key);
    }

    public static String getStandardServiceCode(String propertyKey) {
        String propertyValue = "";
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("config");
            //FileInputStream fin = new FileInputStream("config.properties");
            //Properties p = new Properties();
            //p.load(fin);

            Enumeration enums = bundle.getKeys();
            while (enums.hasMoreElements()) {
                String key = (String) enums.nextElement();
                if (key.equalsIgnoreCase(propertyKey)) {
                    propertyValue = bundle.getString(key);
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return propertyValue;
    }
}