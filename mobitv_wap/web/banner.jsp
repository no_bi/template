<%-- 
    Document   : banner
    Created on : Mar 24, 2017, 10:54:47 AM
    Author     : TILI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div class="banner">
    <div class="container">
        <div class="col-md-12 col-xs-12 top10">
            <div class="row">
                <div class="col-xs-4 col-xs-offset-4" style="position: relative; height: 100px;">
                    <div class="avatar">
                        <img class="img-responsive" src="img/avatar.jpg"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 col-xs-offset-4 text-center top10">
                    <span style="font-weight: bold;">Ngọc Trinh</span>
                </div>
            </div>
            <div class="row top10">
                <div class="col-xs-6 text-center">
                    <span><i class="glyphicon glyphicon-headphones"></i>0912 XXX XXX</span>
                </div>
                <div class="col-xs-6 text-center">
                    <span>Gold hiện có: <i class="glyphicon glyphicon-adjust"></i> 20</span>
                </div>
            </div>
        </div>
    </div>

</div>
