<%-- 
    Document   : index
    Created on : Dec 14, 2016, 4:10:17 PM
    Author     : ManhThang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
              rel = "stylesheet">
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <%--<%@include file="header1.jsp" %>--%>
        <!-- Javascript -->
        <script>
            $(function() {
                $("#dateId").datepicker({
                    beforeShow: function(input, inst) {
                        setTimeout(function() {
                            inst.dpDiv.css({
                                top: 130
                            });
                        }, 0);
                    },
                    buttonImage: 'img/public_items/datepicker_icon.jpg',
                    buttonImageOnly: true,
//                    changeMonth: true,
//                    changeYear: true,
                    showOn: 'button',
                });
                $('#dateId').change(function() {
                    var fromDate = $(this).val();
                    $('#dateIdShow').val(fromDate);
                });

            });


        </script>

    </head>
    <body>
        <%@include file="header.jsp" %>
        <div style="clear: both"></div>
        <%--@include file="layout.jsp" --%>
        <div class="container">  
            <div class="row">
                <div class="col-xs-9" >
                    <div class="row">
                        <div class="col-xs-12" style="padding: 10px">
                            <div class="pull-left title" >LỊCH PHÁT SÓNG</div>                 
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <input id="dateIdShow" readonly="true" class="noneBorder" style="background-color: transparent;color: #777"></input>  
                        </div>
                    </div>
                </div>
                <div class="col-xs-3 myDatepicker" style="padding: 0px">                    
                    <input type="hidden" class="pull-right" id="dateId" style="padding-top:17px;color: #777"></input>                      
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 top10">                    
                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">

                                    <img style="width: 16px;height: 16px;" src="img/public_items/youtube.png" alt=""/>
                                    <!--<a data-toggle="collapse" href="#collapse1">Collapsible panel</a>-->
                                    <span class="pull-right">
                                        <a data-toggle="collapse" href="#collapse1"><i  class="glyphicon glyphicon-menu-down" style="color: #d16d46;"></i></a>
                                    </span>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body" style="padding-top: 0px;">
                                    <div class="row">
                                        <div class="table-responsive">    
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th >Giờ phát sóng</th>
                                                        <th >Chương trình</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>19:00</td>
                                                        <td style="color: #9d2d01">Thời sự</td>                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>19:00</td>
                                                        <td style="color: #9d2d01">Thời sự</td>
                                                    </tr>
                                                    <tr>
                                                        <td>19:00</td>
                                                        <td style="color: #9d2d01">Thời sự</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="panel-footer">Panel Footer</div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>






        </div>        

        <%@include file="footer.jsp" %>

    </body>
    <!--<img class="scrollup" src="img/ico_up.png" >-->

</html>
<script type="text/javascript">

    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".glyphicon-menu-down").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".glyphicon-menu-up").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
    });

</script>