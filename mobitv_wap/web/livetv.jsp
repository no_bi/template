<%-- 
    Document   : index
    Created on : Dec 14, 2016, 4:10:17 PM
    Author     : ManhThang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%--<%@include file="header1.jsp" %>--%>
    <body>
        <%@include file="header.jsp" %>
        <div style="clear: both"></div>
        <%--@include file="layout.jsp" --%>
        <div class="container">
            <div class="row top5">
                <div class="col-xs-3" style="padding: 0px;">

                    <div class="pull-right title" style="margin-top: 9px;"><fmt:message key="cate" bundle="${msg}" /></div>                 

                </div>
                <div class="col-xs-9 " >
                    <!--                    <div class="dropdown" >
                                            <button style="width: 100%;" class="btn btn-default dropdown-toggle "  type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                Kênh phim truyện
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <li><a href="#">Kênh thể thao</a></li>
                                                <li><a href="#">Kênh tổng hợp</a></li>
                                                <li><a href="#">Kênh trẻ em</a></li>                            
                                            </ul>
                                        </div>-->

                    <!--                        <div class="select-side">
                                                <i class="glyphicon glyphicon-menu-down"></i>
                                            </div>-->
                    <select class="form-control" id="sel1">
                        <option>Kênh thể thao</option>
                        <option>Kênh tổng hợp</option>
                        <option>Kênh trẻ em</option>
                    </select>

                </div>
            </div>
            <div class="row top5 padding-right5">
                <div class="col-xs-3 padding1 ">
                    <a title="HTV2" href="http://mobitv.vn/live/272/htv2" >
                        <img class="img-responsive borderRadiusLiveTV"
                             src="img/public_items/icon_tv.png" >
                    </a>
                </div>
                <div class="col-xs-3 padding1">
                    <a title="HTV3" href="http://mobitv.vn/live/275/htv3" >
                        <img class="img-responsive borderRadiusLiveTV" src="img/public_items/icon_tv.png">
                    </a>
                </div>
                <div class="col-xs-3 padding1">
                    <a title="HTV3" href="http://mobitv.vn/live/275/htv3" >
                        <img class="img-responsive borderRadiusLiveTV" src="img/public_items/icon_tv.png">
                    </a>
                </div>
                <div class="col-xs-3 padding1">
                    <a title="HTV3" href="http://mobitv.vn/live/275/htv3" >
                        <img class="img-responsive borderRadiusLiveTV" src="img/public_items/icon_tv.png">
                    </a>
                </div>
            </div>
            <div class="row top5 padding-right5">
                <div class="col-xs-3 padding1">
                    <a title="HTV2" href="http://mobitv.vn/live/272/htv2" >
                        <img class="img-responsive borderRadiusLiveTV"
                             src="img/public_items/icon_tv.png" >
                    </a>
                </div>
                <div class="col-xs-3 padding1">
                    <a title="HTV3" href="http://mobitv.vn/live/275/htv3" >
                        <img class="img-responsive borderRadiusLiveTV" src="img/public_items/icon_tv.png">
                    </a>
                </div>
                <div class="col-xs-3 padding1">
                    <a title="HTV3" href="http://mobitv.vn/live/275/htv3" >
                        <img class="img-responsive borderRadiusLiveTV" src="img/public_items/icon_tv.png">
                    </a>
                </div>
                <div class="col-xs-3 padding1">
                    <a title="HTV3" href="http://mobitv.vn/live/275/htv3" >
                        <img class="img-responsive borderRadiusLiveTV" src="img/public_items/icon_tv.png">
                    </a>
                </div>
            </div>
            <div class="row top5 padding-right5">
                <div class="col-xs-3 padding1">
                    <a title="HTV2" href="http://mobitv.vn/live/272/htv2" >
                        <img class="img-responsive borderRadiusLiveTV"
                             src="img/public_items/icon_tv.png" >
                    </a>
                </div>
                <div class="col-xs-3 padding1">
                    <a title="HTV3" href="http://mobitv.vn/live/275/htv3" >
                        <img class="img-responsive borderRadiusLiveTV" src="img/public_items/icon_tv.png">
                    </a>
                </div>
                <div class="col-xs-3 padding1">
                    <a title="HTV3" href="http://mobitv.vn/live/275/htv3" >
                        <img class="img-responsive borderRadiusLiveTV" src="img/public_items/icon_tv.png">
                    </a>
                </div>
                <div class="col-xs-3 padding1">
                    <a title="HTV3" href="http://mobitv.vn/live/275/htv3" >
                        <img class="img-responsive borderRadiusLiveTV" src="img/public_items/icon_tv.png">
                    </a>
                </div>
            </div>                                                                                                            

            <div style="clear: both"></div>

            <div class="row" >
                <ul class="pager">
                    <li><a href="#" class="mypaper previewpaper"><fmt:message key="next_page" bundle="${msg}" /></a></li>
                    <li><a href="#" class="mypaper nextpaper"><fmt:message key="pre_page" bundle="${msg}" /></a></li>
                </ul>
            </div>                      
            <div style="clear: both"></div>
        </div>

        <%@include file="footer.jsp" %>
    </body>
    <!--<img class="scrollup" src="img/ico_up.png" >-->
</html>

<script type="text/javascript">
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".glyphicon-menu-down").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".glyphicon-menu-up").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
    });
</script>