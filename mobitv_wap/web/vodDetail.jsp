<%-- 
    Document   : index
    Created on : Dec 14, 2016, 4:10:17 PM
    Author     : ManhThang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%--<%@include file="header1.jsp" %>--%>
    <body>
        <%@include file="header.jsp" %>
        <div style="clear: both"></div>
        <%--@include file="layout.jsp" --%>
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <video id="example-video" class= "video-js vjs-default-skin vjs-16-9" controls >
                        <source src=""
                                type="application/x-mpegURL">

                    </video>
                    <script>
                        var player = videojs('example-video');
                        player.play();
                    </script> 
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 title">
                    TÊN PHIM
                </div>
                <div class="col-xs-12" style="color: #000;">Tử địa của Thor - Vikingdom</div>
            </div>
            <div class="row top5">
                <div class="col-xs-12">
                    <div class="img-wrapper">
                        <img class="img-responsive center-block" src="img/public_items/template_11.jpg">
                        <div class="img-overlay">
                            <button class="button-radius" style="color: #fff">Xem phim</button>
                            <button class="button-radius" style="color: #fff">Xem Trailer</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 bold  ">Thể loại: &nbsp;</div>
                <div class="col-xs-8 text-left nonePadding">Hành động viễn tưởng</div>
            </div>
            <div class="row">
                <div class="col-xs-4 bold  ">Đạo diễn: &nbsp;</div>
                <div class="col-xs-8 text-left nonePadding">fantatic</div>
            </div>
            <div class="row">
                <div class="col-xs-4 bold  ">Thời gian: &nbsp;</div>
                <div class="col-xs-8 text-left nonePadding">1 giờ 45 phút</div>
            </div>
            <div class="row">
                <div class="col-xs-12 wrapper">Phim Furious 7 (Quá Nhanh Quá Nguy Hiểm 7) là phần 7 của loạt series Fast & Furious nổi tiếng. Ở cuối phần trước, tưởng chừng như mọi chuyện đã kết thúc, và mở ra một cuộc sống bình lặng, khi cả nhóm đã tiêu diệt được Owen Shaw. Thì trong phần này, sự xuất hiện của Deckard Shaw, người đã giết chết Han, và khiêu chiến với Dominic Toretto, để trả thù cho em trai Owen Shaw của mình, đã làm thay đổi tất cả. Khiến cho cuộc đụng độ giữa 2 băng nhóm lên đến đỉnh điểm. Hai anh em của Paul Walker sẽ là diễn viên đóng thế anh sau khi Paul Walker qua đời.</div>

            </div>
            <div class="row dot" ></div>
            <div class="row">
                <div class="col-xs-6">                            
                    <img src="img/public_items/icon_LIKE_2.gif" alt=""/>
                    <label class="likeVOD" >115 người thích</label>
                </div>
                <div class="col-xs-6">
                    <img src="img/public_items/icon_Report.gif" alt=""/>
                    <label class="likeVOD" >115 người thích</label>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">                            
                    <img src="img/public_items/icon_Report.gif" alt=""/>
                    <label class="likeVOD" >Báo lỗi không xem được</label>
                </div>
            </div>
            <div class="row dot" ></div>
            <div class="row pager">
                <button class="button-radius mypaper" style="color: #fff">Tập 1</button>
                <button class="button-radius mypaper" style="color: #fff">Tập 2</button>
                <button class="button-radius mypaper" style="color: #fff">Tập 3</button>

            </div>
            <div class="row divider-black" ></div>

            <!--PHIM CUNG THE LOAI-->
            <div style="clear: both"></div>

            <div class="row">
                <div class="col-xs-12 title">
                    PHIM CÙNG THỂ LOẠI
                </div>
            </div>

            <div class="row top5">
                <div class="col-xs-6 ">
                    <div class="row">
                        <div class="col-xs-12">
                            <a title="HTV2" href="vodDetail.jsp" >
                                <img class="img-responsive borderWhite"style="width: 100%"
                                     src="img/public_items/icon_vod.jpg" >
                            </a>
                        </div>   
                    </div>   
                    <div class="row">
                        <div class="col-xs-12 nameVOD">
                            Điệp vấn P2-IP man 2
                        </div>
                    </div>
                    <div class="row cateVOD">
                        <div class="col-xs-12">
                            Kiếm hiệp
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">                            
                            <img src="img/public_items/Like_icon.png" alt=""/>
                            <label class="likeVOD" >390</label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-6 ">
                    <div class="row">
                        <div class="col-xs-12 nameVOD">

                            <a title="HTV3" href="vodDetail.jsp" >
                                <img class="img-responsive borderWhite"style="width: 100%" src="img/public_items/icon_vod.jpg" >
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 nameVOD">
                            Cướp biển Caribe
                        </div>
                    </div>
                    <div class="row cateVOD">
                        <div class="col-xs-12">
                            Hoạt hình viễn tưởng
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">                            
                            <img src="img/public_items/Like_icon.png" alt=""/>
                            <label class="likeVOD" >105</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row top5">
                <div class="col-xs-6 ">
                    <div class="row">
                        <div class="col-xs-12">
                            <a title="HTV2" href="vodDetail.jsp" >
                                <img class="img-responsive borderWhite"style="width: 100%"
                                     src="img/public_items/icon_vod.jpg" >
                            </a>
                        </div>   
                    </div>   
                    <div class="row">
                        <div class="col-xs-12 nameVOD">
                            Điệp vấn P2-IP man 2
                        </div>
                    </div>
                    <div class="row cateVOD">
                        <div class="col-xs-12">
                            Kiếm hiệp
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">                            
                            <img src="img/public_items/Like_icon.png" alt=""/>
                            <label class="likeVOD" >390</label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-6 ">
                    <div class="row">
                        <div class="col-xs-12 nameVOD">

                            <a title="HTV3" href="vodDetail.jsp" >
                                <img class="img-responsive borderWhite"style="width: 100%" src="img/public_items/icon_vod.jpg" >
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 nameVOD">
                            Cướp biển Caribe
                        </div>
                    </div>
                    <div class="row cateVOD">
                        <div class="col-xs-12">
                            Hoạt hình viễn tưởng
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">                            
                            <img src="img/public_items/Like_icon.png" alt=""/>
                            <label class="likeVOD" >105</label>
                        </div>
                    </div>
                </div>
            </div>              
            <div class="row">
                <div class="col-xs-12">
                    <ul class="pager">

                        <li><a href="#" class="mypaper nextpaper">Xem toàn bộ ></a></li>
                    </ul>
                </div>
            </div>            
        </div>
        <%@include file="footer.jsp" %>
    </body>
    <!--<img class="scrollup" src="img/ico_up.png" >-->
</html>

<script type="text/javascript">
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".glyphicon-menu-down").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".glyphicon-menu-up").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
    });
</script>