<%-- 
    Document   : index
    Created on : Dec 14, 2016, 4:10:17 PM
    Author     : ManhThang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%--<%@include file="header1.jsp" %>--%>
    <body>
        <%@include file="header.jsp" %>
        <div style="clear: both"></div>
        <%--@include file="layout.jsp" --%>
        <div class="container">            
            
            <div class="row">
                <div class="col-xs-12 title">
                    PHIM HOT
                </div>
            </div>

            <div class="row top5">
                <div class="col-xs-6 ">
                    <div class="row">
                        <div class="col-xs-12 nameVOD">

                            <a title="HTV2" href="vodDetail.jsp" >
                                <img class="img-responsive borderWhite" style="width: 100%"
                                     src="img/public_items/icon_vod.jpg" >
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 nameVOD">
                            Biệt đội siêu anh hùng
                        </div>
                    </div>
                    <div class="row cateVOD">
                        <div class="col-xs-12">
                            Viễn tưởng hành động
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">                            
                            <img src="img/public_items/Like_icon.png" alt=""/>
                            <label class="likeVOD" >115</label>

                        </div>
                    </div>
                </div>

                <div class="col-xs-6 ">
                    <div class="row">
                        <div class="col-xs-12 nameVOD">
                            <a title="HTV3" href="vodDetail.jsp" >
                                <img class="img-responsive borderWhite" src="img/public_items/icon_vod.jpg" style="width: 100%">
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 nameVOD">
                            Công chúa tóc mây
                        </div>
                    </div>
                    <div class="row cateVOD">
                        <div class="col-xs-12">
                            Hoạt hình viễn tưởng
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">                            
                            <img src="img/public_items/Like_icon.png" alt=""/>
                            <label class="likeVOD" >63</label>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row top5">
                <div class="col-xs-6 ">
                    <div class="row">
                        <div class="col-xs-12 nameVOD">

                            <a title="HTV2" href="vodDetail.jsp" >
                                <img class="img-responsive borderWhite" style="width: 100%"
                                     src="img/public_items/icon_vod.jpg" >
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 nameVOD">
                            Biệt đội siêu anh hùng
                        </div>
                    </div>
                    <div class="row cateVOD">
                        <div class="col-xs-12">
                            Viễn tưởng hành động
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">                            
                            <img src="img/public_items/Like_icon.png" alt=""/>
                            <label class="likeVOD" >115</label>

                        </div>
                    </div>
                </div>

                <div class="col-xs-6 ">
                    <div class="row">
                        <div class="col-xs-12 nameVOD">
                            <a title="HTV3" href="vodDetail.jsp" >
                                <img class="img-responsive borderWhite" src="img/public_items/icon_vod.jpg" style="width: 100%">
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 nameVOD">
                            Công chúa tóc mây
                        </div>
                    </div>
                    <div class="row cateVOD">
                        <div class="col-xs-12">
                            Hoạt hình viễn tưởng
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">                            
                            <img src="img/public_items/Like_icon.png" alt=""/>
                            <label class="likeVOD" >63</label>

                        </div>
                    </div>
                </div>
            </div>                
            <div class="row">
                <div class="col-xs-12">
                    <ul class="pager">

                        <li><a href="#" class="mypaper nextpaper"><fmt:message key="view_all" bundle="${msg}" /></a></li>
                    </ul>
                </div>
            </div>            
        </div>
        <%@include file="footer.jsp" %>
    </body>
    <!--<img class="scrollup" src="img/ico_up.png" >-->
</html>

<script type="text/javascript">
    $('.collapse').on('shown.bs.collapse', function() {
        $(this).parent().find(".glyphicon-menu-down").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
    }).on('hidden.bs.collapse', function() {
        $(this).parent().find(".glyphicon-menu-up").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
    });
</script>