<%-- 
    Document   : login
    Created on : Mar 24, 2017, 1:41:23 PM
    Author     : TILI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <%@include file="header.jsp" %>
        <div style="background-color: #FBE647;">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-4 col-xs-offset-4 top100 text-center" style="position: relative; height: 100px;">
                        <div class="avatar">
                            <img class="img-responsive" src="img/avatar.jpg" alt=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 top100 text-center">
                        <button class="btn btnLoginF">FaceBook</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 top10 bottom100 text-center">
                        <!--<button class="btnLoginF">FaceBook</button>-->
                        <button class="btn btnLoginS">Số điện thoại</button>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="footer.jsp" %>
    </body>
</html>
