/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.soft.util;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author tuananhn1411
 */
public class VASGATEService {

    
    public static final String REG_URL = Config.getProperty("REG_URL");
    public static final String UNREG_URL = Config.getProperty("UNREG_URL");
    public static final String CP = Config.getProperty("CP");
    public static final String SERVICE = Config.getProperty("SERVICE");
    public static final String CHANNEL = Config.getProperty("CHANNEL");
    public static final String SECUREPASS = Config.getProperty("SECUREPASS");
    
    public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmmss");

    /**
     * Tạo link đăng ký dịch vụ để redirect sang vasgate
     *
     * @return Link đăng ký
     * @param returnUrl URL sẽ redirect sau khi mua gói thành công, url phải
     * được URL encode và lower
     * @param backUrl URL sẽ redirect khi người dùng muốn quay lại trang cung
     * cấp gói
     * @param packCode MTV|CTV|VTC|XS|PHIM|NHAC|THETHAO|CLIPHOT|VTVCAB
     * @throws java.lang.Exception
     *
     */
    public static String createRegLink(String returnUrl, String backUrl, String packCode) throws Exception {
        StringBuilder sb = new StringBuilder(REG_URL);

        String requestID = System.currentTimeMillis() + "";
        //String _returnUrl = URLEncoder.encode(returnUrl, "UTF-8");
        //String _backUrl = URLEncoder.encode(backUrl, "UTF-8");
        String requestDatetime = SDF.format(new Date());
        String securecode = hash(requestID + returnUrl + backUrl + CP + SERVICE
                + packCode + requestDatetime + CHANNEL + SECUREPASS);

        sb.append("?requestid=").append(requestID);
        sb.append("&returnurl=").append(returnUrl);
        sb.append("&backurl=").append(backUrl);
        sb.append("&cp=").append(CP);
        sb.append("&service=").append(SERVICE);
        sb.append("&package=").append(packCode);
        sb.append("&requestdatetime=").append(requestDatetime);
        sb.append("&channel=" + CHANNEL);
        sb.append("&securecode=").append(securecode);

        return sb.toString();
    }
    public static String createRegLink(String returnUrl, String backUrl, String packCode,String note) throws Exception {
        StringBuilder sb = new StringBuilder(REG_URL);

        String requestID = System.currentTimeMillis() + "";
        //String _returnUrl = URLEncoder.encode(returnUrl, "UTF-8");
        //String _backUrl = URLEncoder.encode(backUrl, "UTF-8");
        String requestDatetime = SDF.format(new Date());
        String securecode = hash(requestID + returnUrl + backUrl + CP + SERVICE
                + packCode + requestDatetime + CHANNEL + SECUREPASS);

        sb.append("?requestid=").append(requestID);
        sb.append("&returnurl=").append(returnUrl);
        sb.append("&backurl=").append(backUrl);
        sb.append("&cp=").append(CP);
        sb.append("&service=").append(SERVICE);
        sb.append("&package=").append(packCode);
        sb.append("&requestdatetime=").append(requestDatetime);
        sb.append("&channel=" + CHANNEL);
        sb.append("&securecode=").append(securecode);
        sb.append("&note=").append(note);
        return sb.toString();
    }

    /**
     * Tạo link hủy dịch vụ để redirect sang vasgate
     *
     * @return Link đăng ký
     * @param returnUrl URL sẽ redirect sau khi hủy gói thành công, url phải
     * được URL encode và lower
     * @param backUrl URL sẽ redirect khi người dùng muốn quay lại trang cung
     * cấp gói
     * @param packCode MTV|CTV|VTC|XS|PHIM|NHAC|THETHAO|CLIPHOT|VTVCAB
     * @throws java.lang.Exception
     *
     */
    public static String createUnRegLink(String returnUrl, String backUrl, String packCode) throws Exception {
        StringBuilder sb = new StringBuilder(UNREG_URL);

        String requestID = System.currentTimeMillis() + "";
        //String _returnUrl = URLEncoder.encode(returnUrl, "UTF-8");
        //String _backUrl = URLEncoder.encode(backUrl, "UTF-8");
        String requestDatetime = SDF.format(new Date());
        String securecode = hash(requestID + returnUrl + backUrl + CP + SERVICE
                + packCode + requestDatetime + CHANNEL + SECUREPASS);

        sb.append("?requestid=").append(requestID);
        sb.append("&returnurl=").append(returnUrl);
        sb.append("&backurl=").append(backUrl);
        sb.append("&cp=").append(CP);
        sb.append("&service=").append(SERVICE);
        sb.append("&package=").append(packCode);
        sb.append("&requestdatetime=").append(requestDatetime);
        sb.append("&channel=" + CHANNEL);
        sb.append("&securecode=").append(securecode);

        return sb.toString();
    }

    public static String hash(String messages) throws Exception {
        if (messages == null) {
            throw new NullPointerException();
        }
        StringBuffer result = null;
        byte[] data = messages.getBytes();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(data);
            byte[] msgDigest = md.digest();
            result = new StringBuffer();
            for (int i = 0; i < msgDigest.length; i++) {
                String hex = Integer.toHexString(0xff & msgDigest[i]);
                if (hex.length() == 1) {
                    result.append('0');
                }
                result.append(hex);
            }
        } catch (NoSuchAlgorithmException ex) {
            throw new Exception(ex.toString());
        }
        return result.toString();
    }
}
