package com.soft.api;

import com.soft.util.Constant;
import com.soft.util.GetJsonData;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ManhThang
 */
public class APICheck {

    public APICheck() {

    }

    public int checkOrder(String msisdn, String id, String type) {
        int check = -1;
        try {
            GetJsonData getJsonCheck = new GetJsonData();
            JSONParser jSONParserCheck = new JSONParser();

            String checkJson = getJsonCheck.readUrl(Constant.URL_CHECK_CONTENT.replace("SDT", msisdn).replace("ID", id).replace("TYPE", type));
            JSONObject jSONObject = (JSONObject) jSONParserCheck.parse(checkJson);
            check = Integer.parseInt(jSONObject.get("status").toString());
//            System.out.println("URL CHECK ===>" + Constant.URL_CHECK_CONTENT.replace("SDT", msisdn).replace("ID", id).replace("TYPE", type));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return check;
    }

    public String post(String para, String url)  {
        
        StringBuffer params = new StringBuffer();

        params.append(para);
        DataOutputStream wr = null;
        BufferedReader buffer = null;
        InputStreamReader ins = null;
        HttpURLConnection con = null;
        StringBuffer response = new StringBuffer();
        try {

            URL obj = new URL(url);
            con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Content-Length", ""
                    + Integer.toString(params.toString().getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");
            con.setUseCaches(false);
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setConnectTimeout(3000);
            con.setReadTimeout(3000);
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            int responseCode = con.getResponseCode();
            ins = new InputStreamReader(con.getInputStream());
            buffer = new BufferedReader(ins);
            String inputLine;
            while ((inputLine = buffer.readLine()) != null) {
                response.append(inputLine);
            }

        } catch (Exception e) {
        } finally {
            try {
                if (con != null) {
                    con.disconnect();
                }
                if (buffer != null) {
                    buffer.close();
                }
                if (ins != null) {
                    ins.close();
                }
                if (wr != null) {
                    wr.close();
                }
            } catch (Exception e) {
            }
        }
        return response.toString();
    }
    
    public static  String love(String msisdn, String id, String type){
        String check="";
        try {
            GetJsonData getJsonCheck = new GetJsonData();
            JSONParser jSONParserCheck = new JSONParser();

            String checkJson = getJsonCheck.readUrl(Constant.URL_LOVE.replace("SDT", msisdn).replace("ID", id).replace("TYPE", type));
            JSONObject jSONObject = (JSONObject) jSONParserCheck.parse(checkJson);
            
            System.out.println("API LOVE" + Constant.URL_LOVE.replace("SDT", msisdn).replace("ID", id).replace("TYPE", type));
//            if(jSONObject.get("status").equals("1")) {
//                check = "Lỗi!";
//            } else {
//                check = "Yêu thích thành công!";
//            }

              check = jSONObject.get("message").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return  check;
    }
    

}
